/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef LCD_H
    #define LCD_H
    
    #include <stdio.h>
    #include <stdlib.h>
    #include <math.h.>
    #include <string.h>
    #include "project.h"
    #include "General.h"
    #include "I2C_Bus.h"
    #include "I2C_LCD.h"
    
    #define LCD_ADDR 0x27
    #define LCD_MAX_COLOUMNS 16
    #define LCD_MAX_ROWS 2
    
    extern int rowCursor;
    
    // Only use these functions for the LCD
    extern void LCD_Setup();
    extern void LCD_Print(char word[]);
    extern void LCD_Set_Cursor(uint8_t, uint8_t);
    extern void LCD_Reset_Cursor();
    extern void LCD_Next_Cursor();
    
#endif

/* [] END OF FILE */
