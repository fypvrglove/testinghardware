/* ========================================
 *
 * Copyright Samuel Walsh, 2014
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Samuel Walsh.
 *
 * ========================================
*/

/* [] END OF FILE */

#ifndef MPU6050_I2C_H
    #define MPU6050_I2C_H

    #include "I2C_Bus.h"

    extern void MPU6050_I2CReadBytes(uint8_t devAddr, uint8_t regAddr, uint8_t length, uint8_t *value);
    extern void MPU6050_I2CReadByte(uint8_t devAddr, uint8_t regAddr, uint8_t *value);
    extern void MPU6050_I2CReadBits(uint8_t devAddr, uint8_t regAddr, uint8_t bitStart, uint8_t length, uint8_t *value);
    extern void MPU6050_I2CReadBit(uint8_t devAddr, uint8_t regAddr, uint8_t bitNum, uint8_t *value);
    extern void MPU6050_I2CWriteBytes(uint8_t devAddr, uint8_t regAddr, uint8_t length, uint8_t *value);
    extern void MPU6050_I2CWriteByte(uint8_t devAddr, uint8_t regAddr, uint8_t value);
    extern void MPU6050_I2CWriteBits(uint8_t devAddr, uint8_t regAddr, uint8_t bitStart, uint8_t length, uint8_t value);
    extern void MPU6050_I2CWriteBit(uint8_t devAddr, uint8_t regAddr, uint8_t bitNum, uint8_t value);
    extern void MPU6050_I2CWriteWords(uint8_t devAddr, uint8_t regAddr, uint8_t length, uint16_t *value);
    extern void MPU6050_I2CWriteWord(uint8_t devAddr, uint8_t regAddr, uint16_t value);

#endif
    