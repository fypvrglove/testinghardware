/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h.>
#include <string.h>
#include "project.h"

// User-defined file includes
#include "StateMachine.h"

void Setup();

int main(void)
{ 
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    Setup();
    Blue_LED_Write(0);
    
    UART_PutString("Hello World\n");
    
    for(;;)
    {
        // Place your application code here. 
        CurrentState();
    }
}

void Setup()
{
    Bluetooth_Setup();
    Buttons_Setup();
    #if LCD_SCREEN_STATE
        LCD_Setup();
    #endif
    #if GYROSCOPE_STATE
        Gyroscope_Setup();
    #endif
}

/* [] END OF FILE */
