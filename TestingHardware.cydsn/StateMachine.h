/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef STATE_H
    #define STATE_H
    
    #include <stdio.h>
    #include <stdlib.h>
    #include <math.h.>
    #include <string.h>
    #include "project.h"
    
    // User-defined file includes
    #include "Buttons.h"
    #include "LCD.h"
    #include "Bluetooth.h"
    #include "Gyroscope.h"
    
    /* Global variables */
    // All possible States
    #define D0_NOTHING 0
    #define TEST_PSOC_BUTTON 1
    #define TEST_LCD_SCREEN 2
    #define TEST_BLUETOOTH 3
    #define TEST_GYROSCOPE 4
    #define TEST_STRAIN_GAUGES 5
    
    #define TEST_BLUETOOTH_GYROSCOPE_DATA_TRANSFER 6
    
    // Current State
    #define STATE TEST_BLUETOOTH
    
    /* --------------- */
    
    /* Function definitions */
    extern void CurrentState();
    extern void DoNothing();
    extern void Test_PSoC_Button();
    extern void TestLCDScreen();
    extern void TestBluetooth();
    extern void TestGyroscope();
    extern void TestStrainGauges();
    
    extern void TestBluetoothGyroscopeDataTransfer();
    /* --------------- */
    
#endif

/* [] END OF FILE */
