/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "General.h"

char* slice_str(char *str, int start, int end) 
{
    int numElements = (end - start + 1);
    int numBytes = sizeof(int) * numElements;

    char *slice = malloc(numBytes);
    memcpy(slice, str + start, numBytes);

    return slice;
}

void remove_new_line(char* str)
{
    for (uint i = 0; i < strlen(str); i++)
    {
        if ( str[i] == '\n' || str[i] == '\r' )
        {
            str[i] = '\0';
        }
    }
}

/* [] END OF FILE */
