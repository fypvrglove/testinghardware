/* ========================================
 *
 * Copyright Samuel Walsh, 2014
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Samuel Walsh.
 *
 * ========================================
*/

/* [] END OF FILE */

#include "mpu6050i2c.h"


void MPU6050_I2CReadBytes(uint8_t devAddr, uint8_t regAddr, uint8_t length, uint8_t *value) {
	#if GYROSCOPE_STATE
        uint8_t i=0;
        I2C_Control_Write(Control_MPU6050);
        I2C_Mux_I2C_MasterSendStart(devAddr, I2C_Mux_I2C_WRITE_XFER_MODE);
    	I2C_Mux_I2C_MasterWriteByte(regAddr);
    	I2C_Mux_I2C_MasterSendRestart(devAddr, I2C_Mux_I2C_READ_XFER_MODE);
    	while (i++ < (length-1)) {
    		*value++ = I2C_Mux_I2C_MasterReadByte(I2C_Mux_I2C_ACK_DATA);
    	}
    	*value = I2C_Mux_I2C_MasterReadByte(I2C_Mux_I2C_NAK_DATA);
    	I2C_Mux_I2C_MasterSendStop();
    #else
        UART_PutString("Change GYROSCOPE_STATE in I2C_Bus.h\n");
    #endif
}

void MPU6050_I2CReadByte(uint8_t devAddr, uint8_t regAddr, uint8_t *value) {
	MPU6050_I2CReadBytes(devAddr, regAddr, 1, value);
}

void MPU6050_I2CReadBits(uint8_t devAddr, uint8_t regAddr, uint8_t bitStart, uint8_t length, uint8_t *value) {
   	uint8_t mask = ((1 << length) - 1) << (bitStart - length + 1);
    MPU6050_I2CReadByte(devAddr, regAddr, value);
    *value &= mask;
    *value >>= (bitStart - length + 1);
}

void MPU6050_I2CReadBit(uint8_t devAddr, uint8_t regAddr, uint8_t bitNum, uint8_t *value) {
	MPU6050_I2CReadByte(devAddr, regAddr, value);
	*value = *value & (1 << bitNum);
}
	
void MPU6050_I2CWriteBytes(uint8_t devAddr, uint8_t regAddr, uint8_t length, uint8_t *value) {
    #if GYROSCOPE_STATE
        uint8_t i=0;
        I2C_Control_Write(Control_MPU6050);
    	I2C_Mux_I2C_MasterSendStart(devAddr, I2C_Mux_I2C_WRITE_XFER_MODE);
    	I2C_Mux_I2C_MasterWriteByte(regAddr);
    	while (i++ < length) {
    		I2C_Mux_I2C_MasterWriteByte(*value++);
    	}
    	I2C_Mux_I2C_MasterSendStop();	
    #else
        UART_PutString("Change GYROSCOPE_STATE in I2C_Bus.h\n");
    #endif
}

void MPU6050_I2CWriteByte(uint8_t devAddr, uint8_t regAddr, uint8_t value) {
	MPU6050_I2CWriteBytes(devAddr, regAddr, 1, &value);
}

void MPU6050_I2CWriteBits(uint8_t devAddr, uint8_t regAddr, uint8_t bitStart, uint8_t length, uint8_t value) {
	uint8_t b;
	uint8_t mask = ((1 << length) - 1) << (bitStart - length + 1);
	MPU6050_I2CReadByte(devAddr, regAddr, &b);
	value <<= (bitStart - length + 1);
	value &= mask;
	b &= ~(mask);
	b |= value;
	MPU6050_I2CWriteByte(devAddr, regAddr, b);	
}

void MPU6050_I2CWriteBit(uint8_t devAddr, uint8_t regAddr, uint8_t bitNum, uint8_t value) {
	uint8_t b;
	MPU6050_I2CReadByte(devAddr, regAddr, &b);
	b = (value != 0) ? (b | (1 << bitNum)) : (b & ~(1 << bitNum));
	MPU6050_I2CWriteByte(devAddr, regAddr, b);
}

void MPU6050_I2CWriteWords(uint8_t devAddr, uint8_t regAddr, uint8_t length, uint16_t *value) {
    #if GYROSCOPE_STATE
        uint8_t i=0;
        I2C_Control_Write(Control_MPU6050);
    	I2C_Mux_I2C_MasterSendStart(devAddr, I2C_Mux_I2C_WRITE_XFER_MODE);
    	I2C_Mux_I2C_MasterWriteByte(regAddr);
    	while (i++ < length) {
    		I2C_Mux_I2C_MasterWriteByte(((uint8_t)*value) >> 8);
    		I2C_Mux_I2C_MasterWriteByte((uint8_t)*value++);
    	}
    	I2C_Mux_I2C_MasterSendStop();
    #else
        UART_PutString("Change GYROSCOPE_STATE in I2C_Bus.h\n");
    #endif
}

void MPU6050_I2CWriteWord(uint8_t devAddr, uint8_t regAddr, uint16_t value) {
	MPU6050_I2CWriteWords(devAddr, regAddr, 1, &value);
}