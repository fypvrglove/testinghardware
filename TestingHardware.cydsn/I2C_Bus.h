/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef I2C_BUS
    #define I2C_BUS
    
    #define Control_LCD 0
    #define Control_MPU6050 1
    
    #define LCD_SCREEN_STATE 0
    #define GYROSCOPE_STATE 0
    
    #include <stdio.h>
    #include <stdlib.h>
    #include <math.h.>
    #include <string.h>
    #include "project.h"
    
    extern int I2C_Bus_Flag;
    
    extern void I2C_Bus_Setup();
    
#endif


/* [] END OF FILE */
