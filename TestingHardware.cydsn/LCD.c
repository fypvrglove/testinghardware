/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "LCD.h"

int rowCursor = 0;

void LCD_Setup()
{
    I2C_Bus_Setup();
    I2C_LCD_init(LCD_ADDR, LCD_MAX_COLOUMNS, LCD_MAX_ROWS, 0);
    I2C_LCD_begin();
    I2C_LCD_clear();
}

void LCD_Print(char word[])
{
    if (rowCursor == 0)
    {
        I2C_LCD_clear();
    }
    int size = strlen(word);
    if (size >= LCD_MAX_COLOUMNS)
    {
        LCD_Reset_Cursor();
        I2C_LCD_print(slice_str(word, 0, LCD_MAX_COLOUMNS-1));
        LCD_Next_Cursor();
        I2C_LCD_print(slice_str(word, LCD_MAX_COLOUMNS, size-1));
    }
    else 
    {
        I2C_LCD_print(word);
        LCD_Next_Cursor();
    }
}

void LCD_Next_Cursor()
{
    if (rowCursor <= (LCD_MAX_ROWS-2))
    {
        rowCursor++;
        LCD_Set_Cursor(0, rowCursor);
    }
    else 
    {
        LCD_Reset_Cursor();
    }
    char Msg[100];
    sprintf(Msg, "rowCursor = %d\n", rowCursor);
    UART_PutString(Msg);
}

void LCD_Reset_Cursor()
{
    rowCursor = 0;
    LCD_Set_Cursor(0,0);
}

void LCD_Set_Cursor(uint8_t i, uint8_t j)
{
    I2C_LCD_setCursor(i,j);
}

/* [] END OF FILE */
