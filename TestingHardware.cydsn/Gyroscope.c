/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "Gyroscope.h"

char buf[200];
char Abuf[200];
char Gbuf[200];
char RPYbuf[200];
int16_t CAX, CAY, CAZ; //current acceleration values
int16_t CGX, CGY, CGZ; //current gyroscope values
int16_t CT;            //current temperature
float   AXoff, AYoff, AZoff; //accelerometer offset values
float   GXoff, GYoff, GZoff; //gyroscope offset values
float   Gyroscope_AX, Gyroscope_AY, Gyroscope_AZ; //acceleration floats
float   Gyroscope_GX, Gyroscope_GY, Gyroscope_GZ; //gyroscope floats
float   Gyroscope_Roll, Gyroscope_Pitch, Gyroscope_Yaw = 0.0f;

void Gyroscope_Setup()
{
    I2C_Bus_Setup();

	MPU6050_init();
	MPU6050_initialize();
    
	UART_PutString(MPU6050_testConnection() ? "MPU6050 connection successful\n\r" : "MPU6050 connection failed\n\n\r");
    UART_PutString("Starting to calibrate values from sensor..\n\r");
    Gyroscope_CalibrateOffsets(); //work out our offsets
}

void Gyroscope_GetMotions()
{
    //Convert values to G's
    MPU6050_getMotion6t(&CAY, &CAX, &CAZ, &CGX, &CGY, &CGZ, &CT);
    Gyroscope_AX = ((float)CAX-AXoff)/16384.00;
    Gyroscope_AY = ((float)CAY-AYoff)/16384.00; //16384 is just 32768/2 to get our 1G value
    Gyroscope_AZ = ((float)CAZ-(AZoff-16384))/16384.00; //remove 1G before dividing

    Gyroscope_GX = ((float)CGX-GXoff)/131.07; //131.07 is just 32768/250 to get us our 1deg/sec value
    Gyroscope_GY = ((float)CGY-GYoff)/131.07;
    Gyroscope_GZ = ((float)CGZ-GZoff)/131.07;

    Gyroscope_Roll  = atan2f(Gyroscope_AY, Gyroscope_AZ) * 180/M_PI;
    Gyroscope_Pitch = atan2f(Gyroscope_AX, sqrt(Gyroscope_AY*Gyroscope_AY + Gyroscope_AZ*Gyroscope_AZ)) * 180/M_PI;
}

void Gyroscope_CalibrateOffsets(void)
{
    int i;
    //Count and average the first n values, defined by numberOfTests above..
    for(i=0; i<numberOfTests; i++)
    {  
        MPU6050_getMotion6t(&CAX, &CAY, &CAZ, &CGX, &CGY, &CGZ, &CT);
        AXoff += CAX;
        AYoff += CAY;
        AZoff += CAZ;
        GXoff += CGX;
        GYoff += CGY;
        GZoff += CGZ;
        CyDelay(25);
    }
    
    AXoff = AXoff/numberOfTests;
    AYoff = AYoff/numberOfTests;
    AZoff = AZoff/numberOfTests;
    GXoff = GXoff/numberOfTests;
    GYoff = GYoff/numberOfTests;
    GZoff = GZoff/numberOfTests;
    
    UART_PutString("\nTest finished, offset values are shown below\n\r");
    sprintf(buf, "\n\rAXoff:%d, AYoff:%d, AZoff:%d || GXoff:%d, GYoff:%d, GZoff:%d,\t\n\r", (int)AXoff,(int)AYoff,(int)AZoff,(int)GXoff,(int)GYoff,(int)GZoff);
    UART_PutString(buf);  
}

/* [] END OF FILE */
