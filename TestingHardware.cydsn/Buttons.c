/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "Buttons.h"

/* Global Variables */
int PSoC_Button_ISR_Flag = 0;
/* ----------------- */

/* Button ISRs */
CY_ISR(PSoC_Button_ISR)
{
    PSoC_Button_ISR_Flag = 1;
}
/* ----------- */

void Buttons_Setup()
{
    PSoC_Button_Interrupt_StartEx(PSoC_Button_ISR);
    
    /* 
    PSoC Button always registers a pulse when the PSoC is restarted (probably noise).
    Hence we reset the button flag after 100 ms to avoid registering this pulse.
    */
    CyDelay(100);
    Buttons_Reset_PSoC_Button_Flag();
}

int Buttons_Get_PSoC_Button_Flag()
{
    if (PSoC_Button_ISR_Flag)
    {
        Buttons_Reset_PSoC_Button_Flag();
        return 1;
    }
    return PSoC_Button_ISR_Flag;
}

void Buttons_Reset_PSoC_Button_Flag()
{
    PSoC_Button_ISR_Flag = 0;
}

/* [] END OF FILE */
