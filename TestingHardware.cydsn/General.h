/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef GENERAL_H
    #define GENERAL_H
    
    #include <stdio.h>
    #include <stdlib.h>
    #include <math.h.>
    #include <string.h>
    #include "project.h"
    
    extern char *slice_str(char *str, int start, int end);
    extern void remove_new_line(char* str);
    
#endif

/* [] END OF FILE */
