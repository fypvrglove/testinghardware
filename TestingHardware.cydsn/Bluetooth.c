/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "Bluetooth.h"

int Bluetooth_Flag = 0;
char Bluetooth_Rx_Buffer[100];
int Bluetooth_Rx_Buffer_Size = 0;
int Bluetooth_Ready_Flag = 0;
char Bluetooth_Data[100];

char gyro_data[200];

void Bluetooth_Setup()
{
    UART_Start();
}

int Bluetooth_Send_Data(char string[])
{
    UART_ClearTxBuffer();
    char outMsg[strlen(string)+10];
    if (string[strlen(string)-1] == '\n')
    {
        sprintf(outMsg, "B: %s#", string);
    }
    else 
    {
        sprintf(outMsg, "B: %s#\n", string);
    }
    UART_PutString(outMsg);
    return 1;
}

char* Bluetooth_Get_Data()
{
    memset(Bluetooth_Data, '\0', sizeof(Bluetooth_Data));
    if (Bluetooth_Data_Avaliable())
    {
        char val = UART_GetChar();
        sprintf(Bluetooth_Rx_Buffer, "%s%c", Bluetooth_Rx_Buffer, val);
        if (Bluetooth_Rx_Buffer[strlen(Bluetooth_Rx_Buffer)-1] == '#')
        {
            strncpy(Bluetooth_Data, Bluetooth_Rx_Buffer, strlen(Bluetooth_Rx_Buffer)-1);
            Bluetooth_Clear_Buffer();
            return Bluetooth_Data;
        }
    }
    return NULL;
}

void Bluetooth_Send_Gyro_Data()
{
    sprintf(Abuf, "A: %.3f,%.3f,%.3f", Gyroscope_AX,Gyroscope_AY,Gyroscope_AZ);
    sprintf(Gbuf, "G: %.3f,%.3f,%.3f", Gyroscope_GX,Gyroscope_GY,Gyroscope_GZ);
    sprintf(RPYbuf, "RPY: %.3f,%.3f,%.3f", Gyroscope_Roll,Gyroscope_Pitch, Gyroscope_Yaw);
    sprintf(gyro_data, "B: %s#B: %s#B: %s#\n", Abuf, Gbuf, RPYbuf);
    UART_PutString(gyro_data);
}

void Bluetooth_Test_Connection()
{
    if (Bluetooth_Flag == 0)
    {
        char* rxData = Bluetooth_Get_Data();
        if (rxData != NULL && strcmp(rxData, "1") == 0)
        {
            Bluetooth_Clear_Buffer();
            Bluetooth_Send_Data("1");
            Bluetooth_Flag = 1;
        }
    }
    if (Bluetooth_Flag == 1)
    {
        char* rxData = Bluetooth_Get_Data();
        if (rxData != NULL && strcmp(rxData, "0") == 0)
        {
            Bluetooth_Clear_Buffer();
            Bluetooth_Send_Data("0");
            Bluetooth_Flag = 2;
        }
    }
    if (Bluetooth_Flag == 2)
    {
        if (Bluetooth_Data_Avaliable())
        {
            char* rxData = Bluetooth_Get_Data();
            if (rxData != NULL && strcmp(rxData, "Ready") == 0)
            {
                Bluetooth_Clear_Buffer();
                Blue_LED_Write(1);
                Bluetooth_Flag = 3;
                Bluetooth_Ready_Flag = 1;
            }
        }
    }
}

void Bluetooth_Test_Transfer_Time()
{
    /*struct timeval stop, start;
    gettimeofday(&start, NULL);
    gettimeofday(&stop, NULL);
    char outMsg[100];
    sprintf(outMsg, "took %lu\n", stop.tv_usec - start.tv_usec);
    UART_PutString(outMsg);*/
    Bluetooth_Send_Gyro_Data();
    CyDelay(10);
}

int Bluetooth_Is_Ready()
{
    return Bluetooth_Ready_Flag;
}

void Bluetooth_Clear_Buffer()
{
    Bluetooth_Rx_Buffer[0] = 0;
    Bluetooth_Rx_Buffer_Size = 0;
}

uint8 Bluetooth_Data_Avaliable()
{
    return UART_GetRxBufferSize();
}

/* [] END OF FILE */
