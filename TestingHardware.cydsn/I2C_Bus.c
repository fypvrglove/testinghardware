/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "I2C_Bus.h"

int I2C_Bus_Flag = 0;

void I2C_Bus_Setup()
{
    if (I2C_Bus_Flag == 0)
    {
        I2C_Mux_I2C_Start();
    }
}

/* [] END OF FILE */
