/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef BLUETOOTH_H
    
    #define BLUETOOTH_H
    
    #include <stdio.h>
    #include <stdlib.h>
    #include <math.h.>
    #include <string.h>
    #include <sys/time.h>
    
    #include "project.h"
    #include "General.h"
    #include "Gyroscope.h"
    
    #define TRANSACTION_END '#'
    #define BLUETOOTH_READY "READY"
    
    extern int Bluetooth_Flag;
    extern int Bluetooth_Ready_Flag;
    extern char Bluetooth_Rx_Buffer[100];
    extern int Bluetooth_Rx_Buffer_Size;
    extern char Bluetooth_Data[100];
    
    extern char gyro_data[200];
    
    extern void Bluetooth_Setup();
    extern int Bluetooth_Send_Data(char string[]);
    extern char* Bluetooth_Get_Data();
    extern void Bluetooth_Test_Connection();
    extern void Bluetooth_Test_Transfer_Time();
    extern int Bluetooth_Is_Ready();
    extern uint8 Bluetooth_Data_Avaliable();
    extern void Bluetooth_Clear_Buffer();
    
    extern void Bluetooth_Send_Gyro_Data();
    
#endif

/* [] END OF FILE */
