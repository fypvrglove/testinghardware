/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef GYROSCOPE_H
    #define GYROSCOPE_H
    
    #include <stdio.h>
    #include <string.h>
    #include "mpu6050.h"
    
    #define numberOfTests   100
    
    extern char Abuf[200];
    extern char Gbuf[200];
    extern char RPYbuf[200];
    extern int16_t CAX, CAY, CAZ; //current acceleration values
    extern int16_t CGX, CGY, CGZ; //current gyroscope values
    extern int16_t CT;            //current temperature
    extern float AXoff, AYoff, AZoff; //accelerometer offset values
    extern float GXoff, GYoff, GZoff; //gyroscope offset values
    extern float Gyroscope_AX, Gyroscope_AY, Gyroscope_AZ; //acceleration floats
    extern float Gyroscope_GX, Gyroscope_GY, Gyroscope_GZ; //gyroscope floats
    extern float Gyroscope_Roll,Gyroscope_Pitch,Gyroscope_Yaw;
    
    extern void Gyroscope_Setup();
    extern void Gyroscope_GetMotions();
    extern void Gyroscope_CalibrateOffsets(void);
    
#endif

/* [] END OF FILE */
