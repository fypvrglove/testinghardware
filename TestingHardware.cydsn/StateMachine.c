/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "StateMachine.h"

void CurrentState()
{
    #if STATE==DO_NOTHING
        DoNothing();
    #endif
    #if STATE==TEST_PSOC_BUTTON
        Test_PSoC_Button();
    #endif
    #if STATE==TEST_LCD_SCREEN 
        TestLCDScreen(); 
    #endif
    #if STATE==TEST_BLUETOOTH 
        TestBluetooth(); 
    #endif
    #if STATE==TEST_GYROSCOPE
        TestGyroscope();
    #endif
    #if STATE==TEST_STRAIN_GAUGES
        TestStrainGauges();
    #endif
    #if STATE==TEST_BLUETOOTH_GYROSCOPE_DATA_TRANSFER
        TestBluetoothGyroscopeDataTransfer();
    #endif
}

void DoNothing()
{
    UART_PutString("Doing Nothing\n");
}

void Test_PSoC_Button()
{
    if (Buttons_Get_PSoC_Button_Flag())
    {
        UART_PutString("PSoC Button Pressed\n");
        Buttons_Reset_PSoC_Button_Flag();
    }
}

void TestLCDScreen()
{
    LCD_Print("Hello World");
    LCD_Print("It works");
    CyDelay(5000);
    LCD_Print("012345678901234567890");
    while (1) {}
}

void TestBluetooth()
{
    if (Bluetooth_Is_Ready() == 0)
    {
        Bluetooth_Test_Connection();
    }
    else
    {
        Bluetooth_Test_Transfer_Time();
    }
}

void TestGyroscope()
{
    Gyroscope_GetMotions();
    if (Buttons_Get_PSoC_Button_Flag())
    {
        sprintf(Abuf, "A: %f,%f,%f\n", Gyroscope_AX,Gyroscope_AY,Gyroscope_AZ);
        sprintf(Gbuf, "G: %f,%f,%f\n", Gyroscope_GX,Gyroscope_GY,Gyroscope_GZ);
        sprintf(RPYbuf, "RPY: %f,%f\n", Gyroscope_Roll,Gyroscope_Pitch);
        UART_PutString(Abuf);
        UART_PutString(Gbuf);
        UART_PutString(RPYbuf);
        Buttons_Reset_PSoC_Button_Flag();
    }
}

void TestStrainGauges()
{
    UART_PutString("Strain Gauges Test not Available\n");
}

void TestBluetoothGyroscopeDataTransfer()
{
    if (Bluetooth_Is_Ready() == 0)
    {
        Bluetooth_Test_Connection();
    }
    else
    {
        Gyroscope_GetMotions();
        if (Buttons_Get_PSoC_Button_Flag())
        {
            Bluetooth_Send_Gyro_Data();
            Buttons_Reset_PSoC_Button_Flag();
        }
    }
}

/* [] END OF FILE */
