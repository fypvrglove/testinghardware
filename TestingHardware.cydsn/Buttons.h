/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef BUTTONS_H
    #define BUTTONS_H
    
    #include <stdio.h>
    #include <stdlib.h>
    #include <math.h.>
    #include <string.h>
    #include "project.h"
    
    /* Global variables */
    extern int PSoC_Button_ISR_Flag;
    /* --------------- */
    
    /* Function definitions */
    extern void Buttons_Setup();
    extern int Buttons_Get_PSoC_Button_Flag();
    extern void Buttons_Reset_PSoC_Button_Flag();
    /* --------------- */
    
#endif

/* [] END OF FILE */
